#!/bin/bash
# usage: <repo_root>/arm-qemu-rpi/runner.sh
# simple script to demonstrate building and running this image...
docker build . -f arm-qemu-rpi/Dockerfile -t arm-qemu-rpi:latest && \
docker run --rm -ti -p 127.0.0.1:2222:2222 --name rpiqemu arm-qemu-rpi:latest $@
