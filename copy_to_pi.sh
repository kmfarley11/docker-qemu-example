#!/bin/bash
# move the source to the pi directly so it can build "natively"...
#
# prerequisites...
#    docker-compose build qemu-user-rpi
#    docker-compose run qemu-user-rpi
#        once login prompts -> pi/raspberry
#            sudo systemctl enable ssh
#            sudo systemctl start ssh
scp -P 2222 -r src/ Makefile pi@192.23.45.4:~/

# afterwards...
#    either in-running container (exec, run, etc.) or via ssh
#        mkdir out
#        make
#        ./out/hello.exe
#        file ./out/hello.exe

