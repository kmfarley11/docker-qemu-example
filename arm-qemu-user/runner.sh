#!/bin/bash
# simple script to demonstrate building and running this image...
# usage: <repo_root>/arm-qemu-user/runner.sh

# note: the 2345 port exposure is for gdb should you wish to remote debug from elsewhere (i.e. the host)
docker build . -f arm-qemu-user/Dockerfile -t arm-qemu-user:latest && \
docker run --rm -ti -p 127.0.0.1:2345:2345 arm-qemu-user:latest $@
