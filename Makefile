# if using the arm toolchain export the commented out lines via arm-env.sh etc...
GCC ?= gcc
SRC ?= src/hello.c
OUT ?= out/hello.exe
#GCC=arm-none-eabi-gcc
#CFLAGS=-I${CHAINLOC}/arm-none-eabi/include/ -g
#SRC=hello-arm.c
#LDFLAGS=-L${CHAINLOC}/arm-none-eabi/lib -lc --specs=nosys.specs -g
#OUT=hello-arm.exe 

$(OUT) : $(SRC)
	$(GCC) $(CFLAGS) $(SRC) $(LDFLAGS) -o $@

clean:
	rm -f $(OUT)
