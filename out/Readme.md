# Mounted folder

This folder (and file) will be shared with the container(s)

So the output of our builds and whatnot should end up here!

_only possible with `docker run -v ./out:/root/out`, or build into docker-compose commands..._
