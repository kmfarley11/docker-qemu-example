#!/bin/bash
# usage: <repo_root>/arm-builder/runner.sh

# simple script to demonstrate building and running this image...
# docker build only takes the directory to build positionally
#   a tag can be provided to access the image rather than using hashes
docker build . -f ./arm-builder/Dockerfile -t arm-builder:latest

# docker run takes the image / tag name primarily
#   --rm will clean up the container after we cleanly exit
#       otherwise the container will hang around until explicitly pruned
#   -v mounts the host directory "mounted" to a location within the container
#   -t allocates a pseudo tty device for interactivity
#   -i creates an interactive session using stdin
docker run --rm -v $(pwd)/out:/root/builder/out -ti arm-builder:latest $@
