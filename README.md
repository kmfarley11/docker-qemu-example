# docker-qemu-example
Scripts and dockerfiles to showcase using containers to build & run embedded toolchains (arm, qemu).

## Required deps
```bash
sudo apt install docker docker.io
sudo groupadd docker
sudo usermod -aG docker ${USER}
```

docker-compose isn't strictly _necessary_, however, it is too nifty to put as optional.
However, we use syntax from the latest. So apt-get wont suffice here, get the latest binary directly:
```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

## Optional deps
To do debugging, you'll want [vscode](https://code.visualstudio.com/) as well as the [remote containers extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers).
Note: the [vscode docker extension](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker) is also recommended!

This particular setup uses gdb and [Native Debug](https://marketplace.visualstudio.com/items?itemName=webfreak.debug) to demonstrate debugging.

## Building & running this source

### Quick(est) start!
```bash
docker-compose run --rm arm-qemu-user
```
That's it... the arm-qemu-user image will use arm-builder to compile hello.c for our arm toolchain. Then arm-qemu-user will pull the executable in and run it with `qemu-arm` in user-space within the container.

For container dev (manually):
```bash
docker-compose --env-file arm-debug.env run --rm arm-dev
cd arm-qemu-user
make clean && make
qemu-arm ./out/hello-arm.exe
```

### Development environment

The environment is configured via `Dockerfile.dev`, `docker-compose.yaml`, and `.devcontainer/`
For information on `.devcontainer/` see [code.visualstudio.com](https://code.visualstudio.com/docs/remote/create-dev-container).

If you open any file in this repo with the proper extensions, you can click the green button in the bottom left and select "Reopen in container". (or use the command pallete: `Ctrl + Shift + P` then search for open/attach container)

It may take a few minutes the first time to get some common dev tools and spin up the image.

Otherwise, you can manually spin up the development container and attach to it through similar means.

After being attached in vs code throught the remote - containers extension:
1. Open src/hello.c and add a breakpoint on the print statement
2. `Ctrl + Shift + B` (or make, make clean && make, etc.)
3. `Ctrl + Shift + P`, then type "Run Task" and hit `Enter`
4. Select the `arm-debug-start` task
4. `F5` (Run > Start Debugging)
    - or: `Ctrl + Shift + D`, click the green play button

If / when you want to open a bash session (outside vscode) to the container
```bash
docker container ls
docker exec -ti docker-qemu-example_arm-dev_1 bash
```

note: you can also debug the remote qemu/container from your host via
```bash
gdb-multiarch -q --ex 'set architecture arm' --ex 'file out/hello-arm.exe' --ex 'target remote 192.23.45.2:2345'
```

if / when you want to cleanly stop the development VM:
```bash
docker-compose down
```

### Other docker-compose commands
```bash
# builds & runs the arm-builder image with variables defined in arm.env
docker-compose --env-file arm.env up arm-builder

# runs the pre-built arm executable via qemu-user, auto-removes container when done
docker-compose run --rm arm-qemu-user

# starts a bash interface into a development container (toolchain, qemu, and gdp ready to go)
docker-compose --env-file arm-debug.env run --rm arm-dev

# runs a raspberry pi via qemu-system-arm (actually demonstrates building qemu too...)
#   DISCLAIMER: this takes a very long time, lots of building and downloading and processing power for this
docker-compose run arm-qemu-rpi

# build & run all images (will take a long time if no images are local...)
docker-compose --profile all up
```

For other details on the individual containers, see the various `Dockerfile`s and `runner.sh`s
The next section discusses using vscode to debug and integrate with the container(s)

## Misc useful commands
```bash
# useful help menus
docker image -h
docker container -h
docker-compose -h

# list current running images, containers etc.
docker image ls
docker container ls
docker ps -a

# clean up dangling containers images etc.
docker system prune

# pull a docker image (from dockerhub by default)
docker pull ubuntu:20.04

# play around (interactively) with a given image, automatically remove container when done
docker run -ti --rm ubuntu:20.04 bash

# run an image in the background, label the instance
docker run --rm -d --name myubuntu ubuntu:20.04 bash

# attach (interactively) to a running container
docker attach myubuntu
# (hold) Ctrl, press P, press Q, release Ctrl to detach

# manually stop a container
docker stop myubuntu

# remove an image manually (useful when the name/tag changes and older ones are not used)
docker image rm ubuntu:latest
```

## Projects to look into...
- https://github.com/dockcross/dockcross
- https://hub.docker.com/r/asmimproved/qemu-mips
